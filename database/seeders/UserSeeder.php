<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'Kevin',
            'email' => 'kevin@gmail.com',
            'password' => Hash::make('12345678'),
        ], [
            'name' => 'Anggara',
            'email' => 'anggara@gmail.com',
            'password' => Hash::make('12345678'),
        ]);
    }
}
