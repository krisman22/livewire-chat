<div>
    <div class="py-12">
        <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
            <div class="overflow-hidden bg-white shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div wire:poll>
                        @foreach ($chat as $chat)
                            <div class="@if ($chat->from_user_id == auth()->id()) chat-end
                            @else
                            chat-start @endif chat">
                                <div class="avatar chat-image">
                                    <div class="w-10 rounded-full">
                                        <img src="https://daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg" alt="Tailwind CSS chat bubble component" />
                                    </div>
                                </div>
                                <div class="chat-header">
                                    {{ $chat->fromUser->name }}
                                    <time class="text-xs opacity-50">{{ $chat->created_at->diffForHumans() }}</time>
                                </div>
                                <div class="chat-bubble">{{ $chat->message }}</div>
                                <div class="chat-footer opacity-50">
                                    Delivered
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-control">
                        <form action="" wire:submit.prevent="sendMessage">
                            <textarea class="textarea textarea-bordered w-full text-white" wire:model="message" placeholder="send your message..."></textarea>
                            <button class="btn btn-primary" type="submit">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
