<p align="center"><a href="https://laravel-livewire.com/" target="_blank"><img src="https://blade-ui-kit.com/images/livewire.svg" width="400" alt="Livewire Logo"></a></p>


## How to Install

Clone project

```shell
git clone https://gitlab.com/krisman22/livewire-chat.git
```

Install composer and npm
```shell
composer install

npm install
```

Copy the `.env.example` file to .env
```shell
cp .env.example .env
```

## Setup Database

Migrate database
```shell
php artisan migrate
```

Seed Database
```shell
php artisan db:seed UserSeeder
```
## How to Use
Access  your app at `http://localhost:8000`.

Run Server and npm run
```shell
php artisan serve

npm run dev
```

For user example
User1 : email - kevin@gmail. password - 12345678.
User2 : email - anggara@gmail. password - 12345678.

